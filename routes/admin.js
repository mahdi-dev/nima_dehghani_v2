var express = require('express');
var router = express.Router();
const User = require('../models/user')
const Event = require('../models/event')
const myFunc = require('../bin/www')
const Question = require('../models/question')

var flag = 0
var flag2 = 0
var message = {
    time: "",
    id: "",
    title: "",
    options: [],
    type: ""
}
var finishAsking = 0
var isPuased = 1
var time
var globalIO = null;

function timer() {
    if (isPuased == 0) {
        time = time - 1
        console.log(time)
    }
    if (time < 0) {
        finishAsking = 1
        isPuased = 1
        flag2 = 0;

    }
}
setInterval(timer, 1000);


//**********Handle GET Requests

router.get('/', ensureAuthenticatedAsAdmin, (req, res) => {
    res.render('../views/AdminLTE-master/index.ejs')
})

//**********EVENT routes
router.get('/addEvent', ensureAuthenticatedAsAdmin, (req, res) => {
    res.render('../views/AdminLTE-master/addEvent.ejs')
})
router.get('/showEvents', ensureAuthenticatedAsAdmin, (req, res) => {
    Event.getAllEvents(function(err, response) {
        if (err) throw err
        res.render('../views/AdminLTE-master/showEvents.ejs', {
            data: response
        })
    })

})
router.get('/enableEvent/:id', (req, res) => {
    Event.getEnableEvent(function(err, response) {
        if (err) throw err
        if (response != null) {
            Event.disableEvent(response._id, function(err, data) {
                if (err) throw err
            })
        }
        Event.enableEvent(req.params.id, function(err, data) {
            if (err) throw err
            res.redirect('/admin/showEvents')
        })
    })

})
router.get('/disableEvent/:id', (req, res) => {
    Event.disableEvent(req.params.id, function(err, data) {
        if (err) throw err
        res.redirect('/admin/showEvents')
    })
})

router.get('/deleteEvent/:id', (req, res) => {
    Event.deleteEvent(req.params.id, function(err, data) {
        if (err) throw err
        res.redirect('/admin/showEvents')
    })
})


//**********Question routes
router.get('/addQuestion', ensureAuthenticatedAsAdmin, (req, res) => {
    res.render('../views/AdminLTE-master/addQuestion.ejs')
})
router.get('/editQuestion/:id', ensureAuthenticatedAsAdmin, (req, res) => {
    Question.getQuestionById(req.params.id, function(err, response) {
        if (err) throw err
        res.render('../views/AdminLTE-master/editQuestion.ejs', {
            data: response
        })
    })

})

router.get('/showQuestions', ensureAuthenticatedAsAdmin, (req, res) => {

    Question.getAllQuestions(function(err, response) {

        if (err) throw err
        res.render('../views/AdminLTE-master/showQuestions.ejs', {
            data: response
        })
    })
})
router.get('/deleteQuestion/:id', ensureAuthenticatedAsAdmin, (req, res) => {
    Question.deleteQuestion(req.params.id, function(err, data) {
        if (err) throw err
        res.redirect('/admin/showQuestions')
    })
})

router.get('/showResult', ensureAuthenticatedAsAdmin, (req, res) => {
        // Question.getResult(req.params.id, function (response) {
        //     console.log(response)
        res.render('../views/AdminLTE-master/showResult.ejs')

        // })
        // res.render('../views/AdminLTE-master/showResult.ejs');

    })
    // res.render('../views/AdminLTE-master/addEvent.ejs')
router.get('/sendQuestion/:id', ensureAuthenticatedAsAdmin, (req, res) => {

    Question.getResult(req.params.id, function(response) {
        console.log("response", response)
        globalIO.sockets.emit("showResult", response)
    })
    Question.getQuestionById(req.params.id, function(err, response) {
        if (err) throw err
        finishAsking = 0;
        flag = 1
        time = response.time + 5
        message.time = response.time
        message.id = response._id
        message.title = response.title
        message.options = response.options
        message.type = response.type
        res.redirect("/admin/showQuestions")
    })
})



//******************************



//**********Handle POST Requests
//**********Event routes
router.post('/addEvent', ensureAuthenticatedAsAdmin, function(req, res) {
    var newEvent = new Event({
        event_title: req.body.title
    })
    Event.createEvent(newEvent, function(err, response) {
        if (err)
            throw err
        res.redirect('/admin/showEvents')
    })
})

function ensureAuthenticatedAsAdmin(req, res, next) {
    if (req.isAuthenticated()) {
        return next()
    }
    res.redirect('/users/login')
}

//**********Question routes
router.post('/addQuestion', ensureAuthenticatedAsAdmin, function(req, res) {

    var newQuestion = new Question({
        title: req.body.title,
        time: req.body.answerTime,
        type: req.body.questionType,
        options: []
    })
    if (req.body.questionType == 1) {
        for (var i = 0; i < req.body.optionNumber; i++) {
            var num = i + 1
            var option = {
                option: req.body['option' + num],
                enable: req.body['option' + num + "radio"]
            }
            newQuestion.options.push(option)
            console.log(newQuestion)
        }
    }
    if (req.body.questionType == 0) {
        var from = {
            option: req.body.fromRange,
            enable: true
        }
        var to = {
            option: req.body.toRange,
            enable: true
        }
        if (req.body.fromRange == undefined) {
            from.option = 0
        }
        if (req.body.toRange == undefined) {
            from.option = 100
        }
        newQuestion.options.push(from)
        newQuestion.options.push(to)
    }
    if (newQuestion.time == null) {
        newQuestion.time = 30
    }
    Question.createQuestion(newQuestion, function(err, response) {
        if (err)
            throw err
        console.log(response)
        res.redirect('/admin/showQuestions')
    })
})
router.post('/editQuestion', ensureAuthenticatedAsAdmin, function(req, res) {
    var newQuestion = new Question({
        _id: req.body._id,
        title: req.body.title,
        time: req.body.answerTime,
        type: req.body.questionType,
        options: []
    })
    if (req.body.questionType == 1) {
        for (var i = 0; i < req.body.optionNumber; i++) {
            var num = i + 1
            var option = {
                option: req.body['option' + num],
                enable: req.body['option' + num + "radio"]
            }
            newQuestion.options.push(option)
            console.log(newQuestion)
        }
    }
    if (req.body.questionType == 0) {
        var from = {
            option: req.body.fromRange,
            enable: true
        }
        var to = {
            option: req.body.toRange,
            enable: true
        }
        newQuestion.options.push(from)
        newQuestion.options.push(to)
    }
    console.log("newwww : + " + req.body.questionType)
    Question.editQuestion(newQuestion, req.body._id, function(err, response) {
        if (err)
            throw err
        res.redirect('/admin/showQuestions')
    })
})

//*********socket*****************
const connections = [];

var x = function(io) {
    io.sockets.on('connection', (socket) => {
        connections.push(socket);
        console.log(' 1 socket is connected. online users : %s', connections.length);
        socket.on('disconnect', () => {
            connections.splice(connections.indexOf(socket), 1);
            console.log(' 1 socket is disconnected . online users : %s', connections.length);
        });
        socket.on("getResult", (id) => {
            Question.getResult(id, function(response) {
                console.log("response", response)
                io.sockets.emit("showResult", response)
            })
        })

    });
    globalIO = io;

    function check() {
        if (flag == 1) {
            time = message.time + 5
            isPuased = 0
            flag = 0
            flag2 = 1
        }
        if (flag2 == 1) {
            // console.log(message)
            message.time = time
            io.sockets.emit('send', message)
        }
    }
    setInterval(check, 100)

    function finishAskingFuck() {
        if (finishAsking == 1) {
            io.sockets.emit("finish", message.id)
        }
    }
    setInterval(finishAskingFuck, 1000)
    return io;
};


module.exports = {
    router,
    x
};