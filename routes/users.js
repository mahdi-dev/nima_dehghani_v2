var express = require('express');
var router = express.Router();
var User = require('../models/user')
const passport = require('passport')
const localStrategy = require('passport-local').Strategy


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

// GET requests of Users 
// router.get('/register', function (req, res, next) {
//     res.render('AdminLTE/pages/examples/register.ejs', {status : 0 , message : 'فرم زیر را تکمیل کنید و ورود بزنید'})
// })

router.get('/login', function (req, res, next) {
    res.render('AdminLTE-master/pages/examples/login.ejs', {status : 0 , message : 'فرم زیر را تکمیل کنید و ورود بزنید'})
})

router.get('/logout', function (req, res, next) {
    req.logout();
    res.redirect('/')
})



//POST request of Users
router.post('/register',function (req, res, next) {
    var username = req.body.username
    var password = req.body.password
    var newUser = new User({
        username : username,
        password : password
    })
    User.createUser(newUser, function (err, user) {
        if(err){
            throw err
        }
        console.log(user)
        res.location('/')
        res.redirect('/')
    })
})


router.post('/login', passport.authenticate('local',{failureRedirect : '/users/login'}),function (req, res, next) {
  res.redirect('/admin')
})

passport.use(new localStrategy(function (username , password , done){
    User.getUserByUsername(username,function (err,user) {
        if(err) throw err
        if(!user){
            console.log("unknowwwwwnnnnn");
            done(null,false,{message : "unknown user"})
        }else{
            User.comparePassword(password , user.password, function (err , isMatch) {
                if(err) return done(err)
                if(isMatch){
                    return done(null, user)
                }else{
                    return done(null,false,{message : "invalid username or password"})
                }
            })
        }

    })
}))

passport.serializeUser(function (user, done) {
    done(null, user.id)
})
passport.deserializeUser(function (id,done) {
    User.getUserById(id, function (err , user) {
        done(err,user)
    })
})


module.exports = router;
