var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
const bodyParser = require('body-parser')
const session = require('express-session')
const passport = require('passport')
const localStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt')
const request = require('request')
const mongo = require('mongodb')
const mongoose = require('mongoose')
var db = mongoose.connection


var app = express();

var http = require('http')
const server = http.createServer(app)




var routes = require('./routes/index').router;
var users = require('./routes/users');
var admin = require('./routes/admin').router;

mongoose.connect('mongodb://nima_dehghani_art_project_admin:a12345678@ds263876.mlab.com:63876/nima_dehghiani_art_project')

var db = mongoose.connection


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, '/views')))
app.use(bodyParser());

app.use('/static', express.static(path.join(__dirname, '/views')))
app.use(express.static(path.join(__dirname, '/views/AdminLTE-master')))

// //Handle Sessions
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}))


// Passport
app.use(passport.initialize())
app.use(passport.session())


//Global Vars
app.get('*', function(req, res, next) {
    // console.log("userrr : " + req.user)
    res.locals.user = req.user || null
    next()
})

// Routes
app.use('/', routes);
app.use('/users', users);
app.use('/admin', admin)


// app.get('*', function (req, res , next) {
//     res.locale.users =  req.users || null
//     next()
// })




// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;