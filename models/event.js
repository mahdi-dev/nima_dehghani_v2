var mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const eventSchema = new mongoose.Schema({
  event_title: {
    type: String
  },
  result: [{
      id: {
      type: mongoose.Schema.ObjectId
    },
    answer: {
      type: String,
      default: 0
    }
  }],
  enable: {
    type: Boolean,
    default: false
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true
  }
})


var Event = module.exports = mongoose.model('Event', eventSchema)

module.exports.createEvent = function (newEvent, callback) {
  newEvent.save(callback)
}

module.exports.getEventById = function (id, callback) {
  Event.findById(id, callback)
}
module.exports.getEnableEvent = function (callback) {
  Event.findOne({
    enable: true
  }, callback)
}

module.exports.getAllEvents = function (callback) {
  Event.find({}, callback)
}

module.exports.deleteEvent = function (eventId, callback) {
  Event.deleteOne({
    _id: eventId
  }, callback)
}



module.exports.enableEvent = function (event_id, callback) {
  Event.updateOne({
    _id: event_id
  }, {
    "$set": {
      enable: true
    }
  }, callback)
}
module.exports.disableEvent = function (event_id, callback) {
  Event.updateOne({
    _id: event_id
  }, {
    "$set": {
      enable: false
    }
  }, callback)
}
module.exports.addResult = function (event_id, questionId,answer, callback) {
  Event.updateOne({
    _id: event_id
  }, {
    "$push": {
      result: {id : questionId , answer : answer}
    }
  }, callback)
}