var mongoose = require('mongoose')
const bcrypt  = require('bcrypt')

// mongoose.connect('mongodb://admin:admin123456@ds343217.mlab.com:43217/event_app')
//
// var db = mongoose.connection


const userSchema = mongoose.Schema({
    password: {
        type: String,
        required: true,
        minlength: 6
    },
    username: {
        type: String,
        index: true,
        unique: true,
        trim: true
    }
}, {
    timestamps: true
})


var User  =  module.exports = mongoose.model('User', userSchema)

module.exports.createUser = function(newUser, callback){
    bcrypt.genSalt(10,function (err, salt) {
        bcrypt.hash(newUser.password, salt, function (err, hash) {
            newUser.password = hash
            newUser.save(callback )
        })
    })
}

module.exports.getUserById = function (id,callback) {
    User.findById(id, callback)
}

module.exports.getUserByEmail = function (email,callback) {
    User.findOne({email : email}, function (err , user) {
        if(err) throw err
        callback(user)
    })
}



module.exports.getUserByUsername = function (username,callback) {
    var query = {username : username}
    User.findOne(query, callback)
}

module.exports.comparePassword = function (candidatePassword, hash , callback) {
    bcrypt.compare(candidatePassword,hash,function (err,isMatch) {
        callback(null , isMatch)
    })
}

module.exports.getHeadOfOrganiser = function (organiser_username , callback) {
    User.findOne({role: 'user' ,is_head_of: true , "organiser_info.organiser_username": organiser_username}, callback)
}
module.exports.getAllHeadsOfUniversity = function ( callback) {
    User.find({role: 'university'}, function (err , data) {
        if(err) throw err
        callback(data)
    })
}


module.exports.remove_head_of_organiser_user = function (id) {
    User.updateOne({_id : id},
        {"$set" : {is_head_of : false ,
                "organiser_info.head_of_organiser_username" : null ,
                "organiser_info.organiser_id" : null,
                "organiser_info.organiser_persian_name" : null,
                "organiser_info.organiser_picture" : null,
                "organiser_info.organiser_category" : null}},
        function (err, data) {
            if(err) throw err
            console.log("dataaaaaaaaaa : ++++ " + JSON.stringify(data))
            console.log("updated successfully")
        })
}
module.exports.set_head_of_organiser_user = function (organiser_info,user_username, callback) {
    User.updateOne({username : user_username} ,{
        "$set" : {
            is_head_of : true,
            organiser_info : organiser_info
        }
    }, function (err ,user) {
        if(err) throw err
       callback(user)
    })
}
