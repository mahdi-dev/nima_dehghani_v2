var mongoose = require('mongoose')
var Event = require('./event')

const questionSchema = new mongoose.Schema({
    id: {
        type: mongoose.Schema.ObjectId
    },
    type: {
        type: Number,
        default: 0
    },
    title: {
        type: String,
    },
    options: [{
        option: {
            type: String
        },
        enable: {
            type: Boolean,
            default: true
        }
    }],
    time: {
        type: Number,
        default: 30
    }
}, {
    timestamps: true,
    toJSON: {
        virtuals: true
    }
})


var Question = module.exports = mongoose.model('Question', questionSchema)

module.exports.createQuestion = function (newQuestion, callback) {
    newQuestion.save(callback)
}
module.exports.getQuestionById = function (id, callback) {
    Question.findById(id, callback)
}
module.exports.getAllQuestions = function (callback) {
    Question.find({}, callback)
}
module.exports.editQuestion = function (question, id, callback) {
    Question.updateOne({
        _id: id
    }, {
        "$set": {
            type: question.type,
            time: question.time,
            title: question.title,
            options: question.options
        }
    }, callback)
}

module.exports.deleteQuestion = function (id, callback) {
    Question.deleteOne({
        _id: id
    }, callback)
}

module.exports.getResult = function (id, callback) {

    Question.findById(id,function(err, response){
        // console.log(response.options);
        if (err) throw err
        Event.getEnableEvent(function (err, res) {
                data = []
                if (err) throw err
                // console.log("kos sher2");
                len = response.options.length;
                answerList = new Array(len);
                answerList.fill(0)
                var count = 0
                res.result.forEach(element => {
                    // console.log(element)
                    if (element.id == id) {
                        
                        
                        // console.log(JSON.stringify(element.answer));
                        if(response.type == 1){
                            for (let i = 0; i < len; i++) {
                                // console.log(element.answer)
                                if(element.answer == ("option"+(i+1))){
                                        // console.log("len=", len);
                                        answerList[i] += 1
                                        break;
                                    }
                            }
                        }
                        if(response.type == 0){
                            count +=1;
                            console.log("element answer : " + parseInt(element.answer))
                            answerList[0] += parseInt(element.answer)
                            answerList[1] = 100
                        }
                    }
                });
            optionList=[]
            if(response.type == 1){
                response.options.forEach(opt => {
                    optionList.push(opt.option)
                })
            }

                // console.log(data.answer);

                if(response.type == 0){
                    answerList[0] = answerList[0] / count
                    optionList.push("average answer")
                }
                callback({
                    id: id,
                    title: response.title,
                    type: response.type,
                    answer: answerList,
                    options: optionList
                });

            })
    });
}